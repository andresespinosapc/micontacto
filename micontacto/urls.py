"""micontacto URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url, include
from django.contrib import admin
from home.views import mics, stories, products, entrepreneurs, \
    profile, myprofile, mic_profile

urlpatterns = [
    url(r'^$', mics),
    url(r'^stories/$', stories),
    url(r'^products/$', products),
    url(r'^entrepreneurs/$', entrepreneurs),
    url(r'^mic/(?P<mic_id>[0-9]+)/$', mic_profile),
    url(r'^profile/(?P<user_id>[0-9]+)/$', profile),
    url(r'^myprofile/$', myprofile),
    url(r'^api/', include('api.urls')),
    url(r'^admin/', admin.site.urls),
    #url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]

if settings.DEBUG is True:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)