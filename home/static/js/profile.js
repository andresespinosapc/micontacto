function setCrsf() {
    // Using csrf
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');
    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
}

// $(document).on('keyup.dismiss.modal', function ( e ) {
//     var a = $('.modal.active').find(':focus');
//     console.log(a);
//     e.which == 27 && $('.modal.active').modal('show');
// });

function setFollowButton() {
    $('#follow-button').removeClass('green').addClass('red').children('i').removeClass().addClass('plus icon')
        .one('click', function() {
            $(this).addClass('loading');
            $.ajax({
                url: '/api/follows/',
                method: 'POST',
                data: {
                    profileId: $('#main-content').attr('user-id')
                }
            }).done(function(msg) {
                $('#follow-button').removeClass('loading');
                setUnfollowButton();
            });
        });
}

function setUnfollowButton() {
    $('#follow-button').removeClass('red').addClass('green').children('i').removeClass().addClass('minus icon')
        .one('click', function() {
            $(this).addClass('loading');
            $.ajax({
                url: '/api/follows/',
                method: 'POST',
                data: {
                    profileId: $('#main-content').attr('user-id'),
                    unfollow: true
                }
            }).done(function(msg) {
                $('#follow-button').removeClass('loading');
                setFollowButton();
            });
        });
}

$(document).ready(function() {
    setCrsf();

    var uploader = $('#fine-uploader');
    uploader.fineUploaderS3({

        multiple: false,
        request: {
            endpoint: 'micontacto.standard.s3.amazonaws.com',
            accessKey: 'AKIAJNNR22TNZ3K6T47Q'
        },
        signature: {
            endpoint: '/api/s3/signature/'
        },
        uploadSuccess: {
            endpoint: '/api/s3/success/'
        },
        deleteFile: {
            enabled: true,
            endpoint: '/api/s3/delete'
        },
        validation: {
            allowedExtensions: ["jpeg", "jpg", "png"],
            acceptFiles: "image/jpeg, image/png",
            sizeLimit: 15000000
        },
        objectProperties: {
            key: function(fileId) {
                var filename = uploader.fineUploader('getName', fileId);
                var uuid = uploader.fineUploader('getUuid', fileId);
                var ext = filename.substr(filename.lastIndexOf('.') + 1);

               return  'mic-logos/' + uuid + '.' + ext;
            }
        }
    });

    var $tags = $('#createmic-form .ui.dropdown');
    $tags.dropdown({
        debug: true,
        placeholder: 'Tags',
        allowAdditions: true,
        hideAdditions: false,
        apiSettings: {
            url: '/api/tags_mic/?input={query}',
            cache: false
        }
    });

    $('#createmic-modal').modal({
        observeChanges: true
    });

    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    $('#imgInput').change(function(){
        readURL(this);
        $('#blah').removeClass('hidden');
        $('#createmic-modal').modal('refresh');
    });

    $.ajax({
        url: '/api/follows',
        method: 'GET',
        data: {
            profileId: $('#main-content').attr('user-id')
        }
    }).done(function(msg) {
        if (!msg.follows) {
            setFollowButton();
        }
        else {
            setUnfollowButton();
        }
    });

    // $('#follow-button').api({
    //     url: '/api/follow/',
    //     method: 'POST',
    //     data: {
    //         user_id: $('#main-content').attr('user-id')
    //     }
    // });

    $('<div class="ui active centered inline loader"></div>').insertAfter('#mic-content .header:first-child');
    $.ajax({
        url: '/api/mics/',
        type: 'GET',
        data: {
            owners: $('#main-content').attr('user-id')
            // mymics: Boolean($('#main-content').attr('my-profile'))
        }
    }).done(function(msg) {
        $('.loader').remove();
        console.log(msg);
        for (var i=0; i<msg.length; i++) {
            var currentMic = msg[i];
            var $item = $('#tools-container').children().eq(0).clone();
            if (currentMic.logo != null) {
                $item.children('img').attr('src', currentMic.logo);
            }
            else {
                $item.children('img').attr('src', 'http://www.edukkit.bligoo.cl/media/users/9/491681/images/public/53240/1304699657608-Ampolleta.jpg');
            }
            $item.find('a.header').text(currentMic.name)
                .attr('href', '/mic/'+ currentMic.id +'/');
            $item.find('div.description').text(currentMic.description);

            $item.appendTo('.ui.list');
        }
    });

    // $('.modal').modal({duration: 0});

    $('.ui.checkbox').checkbox();
    $('#createmic-form').form({
        onSuccess: function(event, fields) {
            $('#createmic-form .submit.button').addClass('loading');

            var current = uploader.fineUploader('getUploads')[0];
            if (current != null) {
                fields.logo_key = uploader.fineUploader('getKey', current.id);
            }

            var tags = [];
            var $tags = $('#createmic-form .ui.dropdown a');
            $tags.each(function() {
                console.log($(this));
                tags.push($(this).attr('data-value'));
            });
            fields.tags = tags;

            fields.anonymous = (fields.anonymous == 'on');

            console.log(fields);

            $.ajax({
                url: '/api/mics/',
                type: 'POST',
                data: fields,
                error: function(msg) {
                    $('#createmic-form .submit.button').removeClass('loading');
                    console.log(msg.responseJSON);
                    // $('#createmic-form').form('add errors', [msg.error]);
                }
            }).done(function(msg) {
                $('#createmic-form .submit.button').removeClass('loading');
                console.log(msg);
                //location.reload();
            });
        }
    });

    $('#createmic-button').click(function() {
        $('#createmic-modal').modal('show');
    });
});
