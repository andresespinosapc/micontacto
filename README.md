# README #

### What is this repository for? ###

This was one of my earliest projects that I just wanted to make public instead of deleting it. 
It served to learn more than anything else, because the project never went forward.

The idea was a entrepreneurship social network where you could post your products and your company details, ambitions, etc. 
It was mainly focused on small and local entrepreneurship, to help people promote their products and find other entrepreneurs to help each other.