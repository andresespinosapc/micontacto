# backend.py in usercp (APP) folder
from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import check_password

# Taken from http://www.djangorocks.com/tutorials/creating-a-custom-authentication-backend/creating-a-simple-authentication-backend.html
class EmailBackend:

    def authenticate(self, email="", password=""):
        try:
            user = get_user_model().objects.get(email=email)
            if check_password(password, user.password):
                return user
            else:
                return None
        except get_user_model().DoesNotExist:
            # No user was found, return None - triggers default login failed
            return None

    # Required for your backend to work properly - unchanged in most scenarios
    def get_user(self, user_id):
        try:
            return get_user_model().objects.get(pk=user_id)
        except get_user_model().DoesNotExist:
            return None