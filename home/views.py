from django.shortcuts import render
from django.contrib.auth import get_user_model
from api.models import Mic, Person

# Create your views here.
def mics(request):
    return render(request, 'mics.html')

def stories(request):
    return render(request, 'stories.html')

def products(request):
    return render(request, 'products.html')

def entrepreneurs(request):
    return render(request, 'entrepreneurs.html')

def mic_profile(request, mic_id):
    mic = Mic.objects.get(id=mic_id)
    is_owner = False
    can_follow = False
    if request.user.is_authenticated():
        person = get_user_model().objects.get(email=request.user).person
        if person in mic.owners.all():
            is_owner = True
        else:
            can_follow = True

    return render(request, 'mic_profile.html', {
        'mic_id': mic.id,
        'mic_name': mic.name,
        'is_owner': is_owner,
        'can_follow': can_follow
    })

def profile(request, user_id, my_profile=False):
    person = Person.objects.get(user=user_id)
    can_follow = False
    if request.user.is_authenticated() and not my_profile:
       can_follow = True

    return render(request, 'profile.html', {
        'my_profile': my_profile,
        'not_my_profile': can_follow,
        'person': person})

def myprofile(request):
    if request.user.is_authenticated():
        user_id = get_user_model().objects.get(email=request.user).id
    else:
        user_id = None

    return profile(request, user_id, my_profile=True)