from django.conf.urls import url
from .views import register, activate
import api.views as api_views

from .views import MicView, ProductView, TmpUploadView, \
    StoryPostView, PersonView, \
    follows
from .views import handle_s3, success_redirect_endpoint, get_tags_mic

urlpatterns = [
    url(r'^auth/register/$', register, name='registration_api_register'),
    url(r'^auth/activate/(?P<activation_key>\w+)/$', activate, name='registration_activate'),
    url(r'^auth/login/$', api_views.AuthView.as_view()),
    url(r'^mics/$', MicView.as_view()),
    url(r'^products/$', ProductView.as_view()),
    url(r'^persons/$', PersonView.as_view()),
    url(r'^story_posts/$', StoryPostView.as_view()),
    url(r'^follows/$', follows),
    url(r'^tmp_upload/$', TmpUploadView.as_view()),
    url(r'^tmp_upload/(?P<qquuid>.*)/$', TmpUploadView.as_view()),
    url(r'^s3/signature/$', handle_s3, name="s3_signee"),
    url(r'^s3/delete', handle_s3, name='s3_delete'),
    url(r'^s3/success/$', success_redirect_endpoint, name="s3_success_endpoint"),
    url(r'^tags_mic/$', get_tags_mic),
]
