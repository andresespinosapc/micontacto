boto==2.42.0
Django==1.9.8
django-custom-user==0.6
django-model-utils==2.5
djangorestframework==3.4.1
mysqlclient==1.3.7
Pillow==3.3.0
pkg-resources==0.0.0
