// Using csrf
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

var fillStory = function() {
    $.ajax({
        url: '/api/story_posts/',
        type: 'GET',
        data: {
            mic: $('#main-content').attr('mic-id')
        }
    }).done(function(msg) {
        $('#posts-container').empty();

        console.log(msg);

        for (var i=0; i<msg.length; i++) {
            var currentPost= msg[i];
            $('#posts-container').append('<div class="ui segment">'+ currentPost.text +'</div>');
        }
    });
}

var fillProducts = function() {

    $.ajax({
        url: '/api/products/',
        type: 'GET',
        data: {
            mic: $('#main-content').attr('mic-id')
        }
    }).done(function(msg) {
        $('.tab[data-tab="mic-products"]').empty();

        var $cards = $('#main-tools-container').children().eq(0).clone()
            .appendTo('.tab[data-tab="mic-products"]');
        if ($('#main-content').attr('is-owner') == 'True') {
            var $add_card = $('#main-tools-container').children().eq(1).clone();
            $add_card.children('.content').remove();
            $add_card.find('img').attr('src', 'http://www.freeiconspng.com/uploads/plus-icon-black-2.png');
            $add_card.click(function() {
                $('#createproduct-modal').modal('show');
            });

            $cards.append($add_card);
        }

        for (var i=0; i<msg.length; i++) {
            var currentProduct= msg[i];
            var $item = $('#main-tools-container').children().eq(1).clone();
            if (currentProduct.main_photo != null) {
                $item.find('img').attr('src', '/' + currentProduct.main_photo_url);
            }
            else {
                $item.find('img').attr('src', 'http://www.edukkit.bligoo.cl/media/users/9/491681/images/public/53240/1304699657608-Ampolleta.jpg');
            }
            $item.find('.header').text(currentProduct.name)
                .attr('href', '/mic/'+ currentProduct.id +'/');
            $item.find('.description').text(currentProduct.description);

            $cards.append($item);
        }
    });
}

var fillGallery = function() {

}

function setFollowButton() {
    $('#follow-button').removeClass('green').addClass('red').children('i').removeClass().addClass('plus icon')
        .one('click', function() {
            $(this).addClass('loading');
            $.ajax({
                url: '/api/follows/',
                method: 'POST',
                data: {
                    profileId: $('#main-content').attr('mic-id')
                }
            }).done(function(msg) {
                $('#follow-button').removeClass('loading');
                setUnfollowButton();
            });
        });
}

function setUnfollowButton() {
    $('#follow-button').removeClass('red').addClass('green').children('i').removeClass().addClass('minus icon')
        .one('click', function() {
            $(this).addClass('loading');
            $.ajax({
                url: '/api/follows/',
                method: 'POST',
                data: {
                    profileId: $('#main-content').attr('mic-id'),
                    unfollow: true
                }
            }).done(function(msg) {
                $('#follow-button').removeClass('loading');
                setFollowButton();
            });
        });
}

$(document).ready(function() {

    $.ajax({
        url: '/api/mics/',
        type: 'GET',
        data: {
            id: $('#main-content').attr('mic-id')
        },
        error: function(msg) {
            console.log('Se ha producido un error');
            console.log(msg);
        }
    }).done(function(msg){
        var tags = msg[0].tags;
        console.log(tags);

        $.each(tags, function(i, tag) {
            $('#tag-column').append('<div class="ui basic label">'+ tag +'</div>');
        });
    });

    $('#createproduct-modal').modal({
        observeChanges: true
    });

    autosize($('textarea'));

    $.ajax({
        url: '/api/follows/',
        method: 'GET',
        data: {
            profileId: $('#main-content').attr('mic-id')
        }
    }).done(function(msg) {
        if (!msg.follows) {
            setFollowButton();
        }
        else {
            setUnfollowButton();
        }
    });

    var storyUploader = $('#fine-uploader-story');
    storyUploader.fineUploaderS3({
        request: {
            endpoint: 'micontacto.standard.s3.amazonaws.com',
            accessKey: 'AKIAJNNR22TNZ3K6T47Q'
        },
        signature: {
            endpoint: '/api/s3/signature'
        },
        uploadSuccess: {
            endpoint: '/api/s3/success'
        },
        deleteFile: {
            enabled: true,
            endpoint: '/api/s3/delete'
        },
        validation: {
            allowedExtensions: ["jpeg", "jpg", "png"],
            acceptFiles: "image/jpeg, image/png",
            sizeLimit: 15000000
        },
        objectProperties: {
            key: function(fileId) {
                var filename = storyUploader.fineUploader('getName', fileId);
                var uuid = storyUploader.fineUploader('getUuid', fileId);
                var ext = filename.substr(filename.lastIndexOf('.') + 1);

               return  'stories/' + uuid + '.' + ext;
            }
        }
    });

    $('#createpost-form').form({
        // beforeSend: function(xhr, settings) {
        //     if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
        //         xhr.setRequestHeader("X-CSRFToken", csrftoken);
        //     }
        //     console.log(csrftoken);
        // },
        onSuccess: function(event, fields) {
            $('#createpost-form .submit.button').addClass('loading');

            var uploads = storyUploader.fineUploader('getUploads');
            fields.photo_keys = [];
            for (var i=0; i<uploads.length; i++) {
                var current = uploads[i];
                fields.photo_keys.push(storyUploader.fineUploader('getKey', current.id));
            }
            console.log(fields.photo_keys);

            $.extend(fields, {micId: $('#main-content').attr('mic-id')});
            console.log(fields);
            $.ajax({
                url: '/api/story_posts/',
                type: 'POST',
                data: fields,
                error: function(msg) {
                    console.log('Se ha producido un error');
                    console.log(msg);
                }
            }).done(function(msg){
                $('#createpost-form .submit.button').removeClass('loading');
                console.log(msg);
                location.reload();
                if (msg.success) {
                    //PENDIENTE
                }
                else {
                    $('#createpost-form').form('add errors', [msg.error]);
                }
            });
        },
        fields: {
            text: {
                identifier: 'text',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'La publicación está vacía.'
                    }
                ]
            }
        }
    });

    var productUploader = $('#fine-uploader-product');
    productUploader.fineUploaderS3({
        multiple: true,
        request: {
            endpoint: 'micontacto.standard.s3.amazonaws.com',
            accessKey: 'AKIAJNNR22TNZ3K6T47Q'
        },
        signature: {
            endpoint: '/api/s3/signature/'
        },
        uploadSuccess: {
            endpoint: '/api/s3/success/'
        },
        deleteFile: {
            enabled: true,
            endpoint: '/api/s3/delete/'
        },
        validation: {
            allowedExtensions: ["jpeg", "jpg", "png"],
            acceptFiles: "image/jpeg, image/png",
            sizeLimit: 15000000
        },
        objectProperties: {
            key: function(fileId) {
                var filename = productUploader.fineUploader('getName', fileId);
                var uuid = productUploader.fineUploader('getUuid', fileId);
                var ext = filename.substr(filename.lastIndexOf('.') + 1);

               return  'products/' + uuid + '.' + ext;
            }
        }
    });

    $('#main-content .menu .item').tab({
        context: '#main-content',
        history: true,
        historyType: 'hash',
        alwaysRefresh: true,
        onLoad: function(tabPath, parameterArray, historyEvent) {
            switch (tabPath) {
                case 'mic-story':
                    fillStory();
                    break;
                case 'mic-products':
                    fillProducts();
                    break;
                case 'mic-gallery':
                    fillGallery();
                    break;
                default:
                    console.log('Invalid tabPath');
            }
        }
    });

    $('#createproduct-form').form({
        // beforeSend: function(xhr, settings) {
        //     if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
        //         xhr.setRequestHeader("X-CSRFToken", csrftoken);
        //     }
        //     console.log(csrftoken);
        // },
        onSuccess: function(event, fields) {
            $('#createproduct-form .submit.button').addClass('loading');

            var uploads = productUploader.fineUploader('getUploads');
            fields.logo_keys = [];
            for (var i=0; i<uploads.length; i++) {
                var current = uploads[i];
                fields.logo_keys.push(productUploader.fineUploader('getKey', current.id));
            }
            console.log(fields.logo_keys);

            $.extend(fields, {mic_id: $('#main-content').attr('mic-id')});
            console.log(fields);
            $.ajax({
                url: '/api/products/',
                type: 'POST',
                data: fields,
                error: function(msg) {
                    console.log('Se ha producido un error');
                    console.log(msg);
                }
            }).done(function(msg){
                $('#createproduct-form .submit.button').removeClass('loading');
                console.log(msg);
                location.reload();
                if (msg.success) {
                    //PENDIENTE
                }
                else {
                    $('#createproduct-form').form('add errors', [msg.error]);
                }
            });
        },
        fields: {
            name: {
                identifier: 'name',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'El producto debe tener un nombre'
                    }
                ]
            },
            price: {
                identifier: 'price',
                optional: true,
                rules: [
                    {
                        type: 'integer[1..]',
                        prompt: 'El precio es inválido'
                    }
                ]
            }
        }
    });

    // $('.menu .item[data-tab="mic-products"]').api({
    //     action: 'get mic products',
    //     urlData: {
    //         mic_id: $('#main-content').attr('mic-id')
    //     },
    //     onSuccess: function (response) {
    //         $('.tab[data-tab="mic-products"]').empty();
    //
    //         var $cards = $('#main-tools-container').children().eq(0).clone()
    //             .appendTo('.tab[data-tab="mic-products"]');
    //         if (Boolean($('#main-content').attr('is-owner'))) {
    //             var $add_card = $('#main-tools-container').children().eq(1).clone();
    //             $add_card.children('.content').remove();
    //             $add_card.find('img').attr('src', 'http://www.freeiconspng.com/uploads/plus-icon-black-2.png');
    //             $add_card.click(function() {
    //                 $('#createproduct-modal').modal('show');
    //             });
    //
    //             $cards.append($add_card);
    //         }
    //     }
    // });
});