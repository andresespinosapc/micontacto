from django.contrib.auth import get_user_model

from rest_framework import serializers
from api import models


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = get_user_model()

    def to_native(self, obj):
        """Remove password field when serializing an object"""
        ret = super(UserSerializer, self).to_native(obj)
        del ret['password']
        return ret


class MicSerializer(serializers.ModelSerializer):
    tags = serializers.SlugRelatedField(
        many=True,
        read_only=True,
        slug_field='tag')

    class Meta:
        model = models.Mic
        extra_kwargs = {'owners': {'read_only': True}}

class ProductSerializer(serializers.ModelSerializer):
    main_photo_url = serializers.SerializerMethodField('get_main_photo')

    class Meta:
        model = models.Product
        extra_kwargs = {'mic': {'read_only': True}}

    def get_main_photo(self, obj):
        if obj.main_photo is not None:
            return obj.main_photo.photo
        else:
            return None


class StoryPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.StoryPost
        extra_kwargs = {'transmitter': {'read_only': True}}


class PersonSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Person
