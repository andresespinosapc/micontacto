// Using csrf
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

// Semantic UI Settings
$.extend($.fn.dropdown.settings, {
    message: {
        addResult: 'Agregar <b>{term}</b>',
        count: '{count} seleccionado(s)',
        maxSelections: 'Máximo {maxCount} selecciones',
        noResults: 'No hay resultados'
    }
});
$.fn.dropdown.settings.keys.delimiter = 32;

function generateUUID() {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;
};

function handleFileComplete(event) {
    console.log(event);
    $('#'+event.item.id).children('#image-loading').empty().removeClass('content').addClass('image').append(event.result);
}

function loadMics(tags) {
    $('#main-content').empty();
    $('#main-content').append('<div id="main-loader" class="ui active centered inline loader"></div>');
    $('#main-tools-container').children().eq(0).clone().appendTo('#main-content');

    var queue = new createjs.LoadQueue();
    queue.addEventListener('fileload', handleFileComplete);

    $.ajax({
        url: '/api/mics/',
        type: 'GET',
        data: {
            tags: tags
        }
    }).done(function(msg) {
        $('#main-loader').remove();
        for (var i=0; i<msg.length; i++) {
            var currentCard = msg[i];
            var $card = $('#main-tools-container').children().eq(1).clone();
            // $card.find('img').attr('src', 'https://upload.wikimedia.org/wikipedia/commons/2/28/Cerro_La_Campana.jpg');
            // $card.find('#image-loading').append('')
            var uuid = generateUUID();
            $card.attr('id', uuid);
            queue.loadFile({
                id: uuid,
                src: 'https://upload.wikimedia.org/wikipedia/commons/2/28/Cerro_La_Campana.jpg'
            });

            $card.find('.header').text(currentCard.name);
            $card.find('.description').text(currentCard.description);
            $card.attr('href', '/mic/'+ currentCard.id +'/');

            $card.appendTo('#card-container');
        }
    });
}

$(document).ready(function() {
    var $searchBar = $('#main-searchbar .ui.dropdown');
    $searchBar.dropdown({
        // debug: true,
        placeholder: 'Buscar MiCs...',
        apiSettings: {
            url: '/api/tags_mic/?input={query}',
            cache: false
        },
        onChange: function(values) {
            console.log('onchage');
            loadMics(values);
        },
        onRemove: function() {
            console.log('removed');
            var values = $searchBar.dropdown('get value');
            console.log(values);
        }
    });

    $('#signup-modal').modal({
        observeChanges: true
    });
    $('#login-modal').modal({
        observeChanges: true
    });

    $('#signup-button').click(function () {
        $('#signup-modal').modal('show');
    });

    $('#login-button').click(function () {
        $('#login-modal').modal('show');
    });

    $('#logout-button').click(function () {
        $.ajax({
            url: '/api/auth/login/',
            type: 'DELETE'
        }).done(function (msg) {
            console.log(msg);
            location.reload();
        });
    });

    $('#login-form').form({
        onSuccess: function(event, fields) {
            $('#login-form .submit.button').addClass('loading');
            $.ajax({
                url: '/api/auth/login/',
                type: 'POST',
                data: fields
            }).done(function(msg) {
                $('#login-form .submit.button').removeClass('loading');
                if (msg.success) {
                    location.reload();
                }
                else {
                    $('#login-form').form('add errors', [msg.error]);
                }
            });
        },
        transition: 'slide down',
        duration: 500,
        fields: {
            email: {
                identifier: 'email',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Por favor ingresa tu correo'
                    },
                    {
                        type: 'email',
                        prompt: 'El email es inválido'
                    }
                ]
            },
            password: {
                identifier: 'password',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Por favor ingresa tu contraseña'
                    }
                ]
            }
        }
    });

    $('#signup-form').form({
        onSuccess: function(event, fields) {
             
            $.ajax({
                url: '/api/auth/register/',
                type: 'POST',
                data: fields,
                error: function(msg) {
                    console.log(msg);
                }
            }).done(function(msg) {
                $('#signup-form .submit.button').removeClass('loading');
                console.log(msg);
                if (msg.success) {
                    //PENDIENTE
                }
                else {
                    $('#signup-form').form('add errors', [msg.error]);
                }
            });
        },

        transition: 'slide down',
        duration: 500,
        fields: {
            first_name: {
              identifier: 'first_name',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Por favor ingresa tu nombre'
                    }
                ]
            },
            last_name: {
                identifier: 'last_name',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Por favor ingresa tus apellidos'
                    }
                ]
            },
            email: {
                identifier: 'email',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Por favor ingresa tu correo'
                    },
                    {
                        type: 'email',
                        prompt: 'Ingresa un email válido'
                    }
                ]
            },
            password: {
                identifier: 'password',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Por favor ingresa tu contraseña'
                    },
                    {
                        type: 'minLength[8]',
                        prompt: 'Tu contraseña debe tener al menos 8 caracteres'
                    },
                    {
                        type: 'regExp[[a-z]]',
                        prompt: 'Tu contraseña debe contener al menos una letra'
                    },
                    {
                        type: 'regExp[[0-9]]',
                        prompt: 'Tu contraseña debe contener al menos un dígito'
                    }
                ]
            }
        }
    });
});