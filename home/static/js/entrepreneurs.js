// Using csrf
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

$(document).ready(function() {
    $('#main-content').append('<div class="ui active centered inline loader"></div>');
    $('#main-tools-container').children().eq(0).clone().appendTo('#main-content');

    $.ajax({
        url: '/api/persons/',
        type: 'GET'
    }).done(function(msg) {
        $('.loader').remove();
        for (var i=0; i<msg.length; i++) {
            var currentCard = msg[i];
            var $card = $('#main-tools-container').children().eq(1).clone();
            $card.find('img').attr('src', 'https://upload.wikimedia.org/wikipedia/commons/2/28/Cerro_La_Campana.jpg');
            $card.find('.header').text(currentCard.first_name + ' ' + currentCard.last_name);
            $card.find('.description').text(currentCard.description);
            $card.attr('href', '/profile/'+ currentCard.user +'/');

            $card.appendTo('#card-container');
        }
    });
});
