import datetime

from django.conf import settings
from django.db import models
from django.utils.timezone import now as datetime_now
from django.utils.translation import ugettext_lazy as _

from model_utils.managers import InheritanceManager


class RegistrationProfile(models.Model):
    """
    A simple profile which stores an activation key for use during
    user account registration.

    """
    ACTIVATED = u"ALREADY_ACTIVATED"

    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        unique=True,
        verbose_name=_('user'),
        related_name='api_registration_profile')
    activation_key = models.CharField(_('activation key'), max_length=40)

    def activation_key_expired(self):
        """
        Determine whether this ``RegistrationProfile``'s activation
        key has expired, returning a boolean -- ``True`` if the key
        has expired.
        Key expiration is determined by a two-step process:
        1. If the user has already activated, the key will have been
        reset to the string constant ``ACTIVATED``. Re-activating
        is not permitted, and so this method returns ``True`` in
        this case.

        2. Otherwise, the date the user signed up is incremented by
        the number of days specified in the setting
        ``REGISTRATION_API_ACCOUNT_ACTIVATION_DAYS`` (which should be
        the number of days after signup during which a user is allowed
        to activate their account); if the result is less than or
        equal to the current date, the key has expired and this method
        returns ``True``.

        """

        # utils imported here to avoid circular import
        from . import utils

        expiration_date = datetime.timedelta(
            days=utils.get_settings('REGISTRATION_API_ACCOUNT_ACTIVATION_DAYS'))
        return self.activation_key == self.ACTIVATED or \
            (self.user.date_joined + expiration_date <= datetime_now())


class Entity(models.Model):
    pass


class Person(Entity):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, primary_key=True)
    first_name = models.CharField(max_length=100, default=None)
    last_name = models.CharField(max_length=100, default=None)
    profile_photo = models.URLField(default=None, null=True, blank=True)
    gender = models.CharField(max_length=1, default=None, null=True, blank=True)
    birth_date = models.DateField(default=None, null=True, blank=True)
    # follows_person = models.ManyToManyField('api.Person')
    # follows_mic = models.ManyToManyField('api.Mic')
    follows = models.ManyToManyField(Entity, related_name='follower')


class Mic(Entity):
    owners = models.ManyToManyField(Person, blank=False)
    name = models.CharField(max_length=30, unique=True)
    logo = models.URLField(default=None, null=True, blank=True)
    description = models.CharField(max_length=500, null=True, blank=True)
    anonymous = models.BooleanField()
    tags = models.ManyToManyField('api.TagMic', blank=True)

# class Person(models.Model):
#     user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, primary_key=True)
#     first_name = models.CharField(max_length=100, default=None)
#     last_name = models.CharField(max_length=100, default=None)
#     profile_photo = models.URLField(default=None, null=True, blank=True)
#     gender = models.CharField(max_length=1, default=None, null=True, blank=True)
#     birth_date = models.DateField(default=None, null=True, blank=True)
#     follows_person = models.ManyToManyField('api.Person')
#     follows_mic = models.ManyToManyField('api.Mic')
#
#
# class Mic(models.Model):
#     owners = models.ManyToManyField(Person)
#     name = models.CharField(max_length=30, unique=True)
#     logo = models.URLField(default=None, null=True, blank=True)
#     description = models.CharField(max_length=500, null=True, blank=True)
#     anonymous = models.BooleanField()
#     tags = models.ManyToManyField('api.TagMic')


class Product(models.Model):
    mic = models.ForeignKey(Mic, on_delete=models.CASCADE)
    name = models.CharField(max_length=30)
    description = models.CharField(max_length=500, null=True, blank=True)
    price = models.PositiveIntegerField(null=True)
    main_photo = models.OneToOneField('api.ProductPhoto', null=True, related_name='product_main')
    tags = models.ManyToManyField('api.TagProduct', blank=True)


class ProductPhoto(models.Model):
    product = models.ForeignKey('api.Product', on_delete=models.CASCADE)
    photo = models.URLField()


class StoryPost(models.Model):
    # objects = InheritanceManager()

    transmitter = models.ForeignKey(Entity, on_delete=models.CASCADE)
    text = models.CharField(max_length=500, null=False, blank=False)
    date = models.DateTimeField(auto_now_add=True)

# class StoryPostMic(StoryPost):
#     mic = models.ForeignKey(Mic, on_delete=models.CASCADE)
#
# class StoryPostPerson(StoryPost):
#     person = models.ForeignKey(Person, on_delete=models.CASCADE)


class StoryPhoto(models.Model):
    post = models.ForeignKey(StoryPost, on_delete=models.CASCADE)
    photo = models.URLField()

# class StoryPhotoMic(models.Model):
#     post = models.ForeignKey(StoryPostMic, on_delete=models.CASCADE)
#     photo = models.URLField()
#
# class StoryPhotoPerson(models.Model):
#     post = models.ForeignKey(StoryPostPerson, on_delete=models.CASCADE)
#     photo = models.URLField()


class TagMic(models.Model):
    tag = models.CharField(max_length=20, unique=True)


class TagProduct(models.Model):
    tag = models.CharField(max_length=20, unique=True)