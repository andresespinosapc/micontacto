from rest_framework import permissions


class IsOwnerOrReadOnly(permissions.BasePermission):
    message = 'You are not owner of this resource'

    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True

        return False