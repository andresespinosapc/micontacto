from django.http import HttpResponseRedirect, HttpResponse

from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from . import utils

from rest_framework.views import APIView
from django.contrib.auth import get_user_model
from django.contrib.auth import login, logout, authenticate

from rest_framework import generics
from . import models, serializers
from .permissions import IsOwnerOrReadOnly
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAuthenticated

from django.db.models import Count, Q
from django.conf import settings
import os
from django.core.files import File

import operator
from functools import reduce

import json, base64, hmac, hashlib
from django.views.decorators.csrf import csrf_exempt
try:
    import boto
    from boto.s3.connection import Key, S3Connection
    boto.set_stream_logger('boto')
    S3 = S3Connection(settings.AWS_SERVER_PUBLIC_KEY, settings.AWS_SERVER_SECRET_KEY)
except ImportError:
    print("Could not import boto, the Amazon SDK for Python.")
    print("Deleting files will not work.")
    print("Install boto with")
    print("$ pip install boto")

VALID_USER_FIELDS = utils.get_valid_user_fields()


@api_view(['POST'])
@permission_classes((AllowAny,))
def register(request):
    serialized = serializers.UserSerializer(data=request.POST)
    if serialized.is_valid():
        user_data = utils.get_user_data(request.POST)
        utils.create_inactive_user(**user_data)
        return Response(utils.USER_CREATED_RESPONSE_DATA,
                        status=status.HTTP_201_CREATED)
    else:
        return Response(serialized._errors, status=status.HTTP_400_BAD_REQUEST)


def activate(request, activation_key=None):
    """
    Given an an activation key, look up and activate the user
    account corresponding to that key (if possible).

    """
    utils.activate_user(activation_key)
    # if not activated
    success_url = utils.get_settings('REGISTRATION_API_ACTIVATION_SUCCESS_URL')
    if success_url is not None:
        return HttpResponseRedirect(success_url)


class ListAPIFilteredView(generics.ListCreateAPIView):
    """
    View that allows you to filter through url params and defined fields
    """

    def get_queryset(self):
        queryset = super(ListAPIFilteredView, self).get_queryset()
        query_params = self.request.query_params
        params = query_params.keys()
        query_filters = []
        custom_filters = []

        try:
            query_filters = self.filter_fields
        except AttributeError:
            pass

        try:
            custom_filters = self.custom_filters
        except AttributeError:
            pass

        for field in query_filters:
            if field in params:
                value = query_params.getlist(field)

                if not value:
                    continue

                if len(value) > 1:
                    s = {'%s__in' % field: value}
                else:
                    s = {field: value[0]}
                queryset = queryset.filter(**s)

        for field in custom_filters:
            if field in params:
                try:
                    queryset = getattr(self, "filter_%s" % field)(queryset)
                except AttributeError:
                    continue

        return queryset


class AuthView(APIView):
    permission_classes = (AllowAny,)
    queryset = get_user_model().objects.none()

    def post(self, request, *args, **kwargs):
        email = request.POST['email']
        password = request.POST['password']
        user = authenticate(email=email, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                # Redirect to a success page.
                return Response({'success': True})
            else:
                # Return a 'disabled account' error message
                return Response({'success': False, 'error': 'Cuenta inactiva'})
        else:
            # Return an 'invalid login' error message.
            return Response({'success': False, 'error': 'Email o contraseña invalidos'})

    def delete(self, request, *args, **kwargs):
        logout(request)
        return Response({'success': True})


class TmpUploadView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        uploaded_file = request.FILES.get('qqfile')
        tmp_dir = os.path.join(settings.MEDIA_ROOT, 'tmp')
        filename = '{}!{}'.format(request.POST['qquuid'], request.POST['qqfilename'])
        dest_path = os.path.join(tmp_dir, filename)

        try:
            with open(dest_path, 'xb+') as dest_file:
                for chunk in uploaded_file.chunks():
                    dest_file.write(chunk)

            return Response({'success': True, 'filename': filename})
        except FileExistsError:
            return Response({'success': False, 'error': 'File already exists'})

    def delete(self, request, *args, **kwargs):
        tmp_dir = os.path.join(settings.MEDIA_ROOT, 'tmp')
        filename = '{}!{}'.format(kwargs['qquuid'], request.GET['qqfilename'])
        dest_path = os.path.join(tmp_dir, filename)
        os.remove(dest_path)

        return Response({'success': True})


class MicView(ListAPIFilteredView):
    # queryset = models.Mic.objects.all()
    serializer_class = serializers.MicSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)
    filter_fields = ('owners', 'id')
    #custom_filters = ('mymics',)
    # custom_filters = ('tags',)

    # def filter_mymics(self, queryset):
    #     return queryset.filter(owners__user__email=self.request.user)

    # def filter_tags(self, queryset):
    #     return queryset.filter(tags__tag=self.request.GET['tags'])

    def get_queryset(self):
        tags = self.request.GET.getlist('tags[]')
        print(tags)
        # if len(tags) > 0:
        #     return models.Mic.objects.filter(tags__tag__in=tags).annotate(num_tags=Count('tags')).filter(num_tags=len(tags))
        # else:
        #     return models.Mic.objects.all()

        if len(tags) > 0:
            self.queryset = models.Mic.objects.filter(tags__tag__in=tags).annotate(num_tags=Count('tags')).filter(num_tags=len(tags))
        else:
            self.queryset = models.Mic.objects.all()

        return super().get_queryset()

    def perform_create(self, serializer):
        logo_key = self.request.POST.get('logo_key')
        if logo_key is not None:
            logo_url = os.path.join('https://s3.amazonaws.com/micontacto.standard', )
        else:
            logo_url = None

        tags = self.request.POST.getlist('tags[]')
        tagObjects = []
        for tag in tags:
            tagObjects.append(models.TagMic.objects.get_or_create(tag=tag)[0].id)

        serializer.save(
            logo=logo_url,
            tags=tagObjects,
            owners=[models.Person.objects.get(user__email=self.request.user).user_id])

        # logo_dir = os.path.join(settings.MEDIA_ROOT, 'tmp', self.request.POST['logo_filename'])
        # with open(logo_dir, 'rb') as f:
        #     logo_file = File(f)
        #
        #     serializer.save(
        #         logo=logo_file,
        #         owners=[models.Person.objects.get(user__email=self.request.user).user_id])
        #
        # os.remove(logo_dir)


class ProductView(ListAPIFilteredView):
    queryset = models.Product.objects.all()
    serializer_class = serializers.ProductSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)
    filter_fields = ('mic',)

    def perform_create(self, serializer):
        mic_id = self.request.POST['mic_id']
        mic = models.Mic.objects.get(id=mic_id)
        user = models.Person.objects.get(user__email=self.request.user)
        if mic in user.mic_set.all():
            # Is owner
            new_product = serializer.save(mic=mic)
            # Main photo
            logo_key = self.request.POST.getlist('logo_keys[]')[0]
            logo_url = os.path.join('https://s3.amazonaws.com/micontacto.standard', logo_key)
            new_photo = models.ProductPhoto.objects.create(
                product=new_product, photo=logo_url)
            new_product.main_photo = new_photo
            new_product.save()

            for logo_key in self.request.POST.getlist('logo_keys[]')[1:]:
                logo_url = os.path.join('https://s3.amazonaws.com/micontacto.standard', logo_key)
                models.ProductPhoto.objects.create(
                    product=new_product, photo=logo_url)


class StoryPostView(ListAPIFilteredView):
    queryset = models.StoryPost.objects.order_by('-date').all()
    serializer_class = serializers.StoryPostSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def perform_create(self, serializer):
        mic_id = self.request.POST.get('micId')
        if mic_id is not None:
            mic = models.Mic.objects.get(id=mic_id)
            user = models.Person.objects.get(user__email=self.request.user)
            if mic in user.mic_set.all():
                # Is owner
                new_post = serializer.save(transmitter=mic)

                for photo_key in self.request.POST.getlist('logo_keys[]'):
                    photo_url = os.path.join('https://s3.amazonaws.com/micontacto.standard', photo_key)
                    models.StoryPhoto.objects.create(
                        post=new_post, photo=photo_url)
        else:
            person = models.Person.objects.get(user__email=self.request.user)
            serializer.save(transmitter=person)


class PersonView(ListAPIFilteredView):
    queryset = models.Person.objects.all()
    serializer_class = serializers.PersonSerializer
    permission_classes = (AllowAny,)


@api_view(['POST', 'GET'])
@permission_classes((IsAuthenticated,))
def follows(request):
    person = models.Person.objects.get(user__email=request.user)
    if request.method == 'POST':
        follows_id = request.POST.get('profileId')
        unfollow = False
        if request.POST.get('unfollow') == 'true':
            unfollow = True
        if follows_id is not None:
            if not unfollow:
                person.follows.add(follows_id)
            else:
                person.follows.remove(follows_id)
            return Response({'success': True})
        else:
            return Response({
                'success': False,
                'error': 'profileId not given'})
    elif request.method == 'GET':
        follows_id = request.GET.get('profileId')
        if follows_id is not None:
            if person.follows.filter(follower=follows_id).exists():
                return Response({'success': True, 'follows': True})
            else:
                return Response({'success': True, 'follows': False})
        else:
            return Response({
                'success': True,
                'follows': person.follows.all()})

# @api_view(['GET'])
# @permission_classes((IsAuthenticated,))
# def get_stories(request):
#     person = models.Person.objects.get(user__email=request.user)
#
#     mic_query = models.StoryPostMic.objects.filter(mic__in=person.follows_mic.iterator())
#     person_query = models.StoryPostPerson.objects.filter(person__in=person.follows_person.iterator())
#
#     return Response({})


@api_view(['GET'])
@permission_classes((AllowAny,))
def get_tags_mic(request):
    inp = request.GET['input']
    if len(inp) < 3:
        return Response({})
    else:
        query = models.TagMic.objects.filter(tag__startswith=inp).values_list('tag', flat=True)[:20]
        results = map(lambda x: {'name': x, 'value': x}, query)
        return Response({
            'success': True,
            'results': results
        })



# FineUploader

@csrf_exempt
def success_redirect_endpoint(request):
    """ This is where the upload will snd a POST request after the
    file has been stored in S3.
    """
    return make_response(200)


@csrf_exempt
def handle_s3(request):
    """ View which handles all POST and DELETE requests sent by Fine Uploader
    S3. You will need to adjust these paths/conditions based on your setup.
    """
    if request.method == "POST":
        return handle_POST(request)
    elif request.method == "DELETE":
        return handle_DELETE(request)
    else:
        return HttpResponse(status=405)


def handle_POST(request):
    """ Handle S3 uploader POST requests here. For files <=5MiB this is a simple
    request to sign the policy document. For files >5MiB this is a request
    to sign the headers to start a multipart encoded request.
    """
    if request.POST.get('success', None):
        return make_response(200)
    else:
        request_payload = json.loads(request.body.decode())
        headers = request_payload.get('headers', None)
        if headers:
            # The presence of the 'headers' property in the request payload
            # means this is a request to sign a REST/multipart request
            # and NOT a policy document
            response_data = sign_headers(headers)
        else:
            if not is_valid_policy(request_payload):
                return make_response(400, {'invalid': True})
            response_data = sign_policy_document(request_payload)
        response_payload = json.dumps(response_data)

        return make_response(200, response_payload)


def handle_DELETE(request):
    """ Handle file deletion requests. For this, we use the Amazon Python SDK,
    boto.
    """
    if boto:
        bucket_name = request.GET.get('bucket')
        key_name = request.GET.get('key')
        aws_bucket = S3.get_bucket(bucket_name, validate=False)
        aws_key = Key(aws_bucket, key_name)
        aws_key.delete()
        return make_response(200)
    else:
        return make_response(500)


def make_response(status=200, content=None):
    """ Construct an HTTP response. Fine Uploader expects 'application/json'.
    """
    response = HttpResponse()
    response.status_code = status
    response['Content-Type'] = "application/json"
    response.content = content
    return response


def is_valid_policy(policy_document):
    """ Verify the policy document has not been tampered with client-side
    before sending it off.
    """
    bucket = ''
    parsed_max_size = 0

    for condition in policy_document['conditions']:
        if isinstance(condition, list) and condition[0] == 'content-length-range':
            parsed_max_size = condition[2]
        else:
            if condition.get('bucket', None):
                bucket = condition['bucket']

    return bucket == settings.AWS_EXPECTED_BUCKET and parsed_max_size == settings.AWS_MAX_SIZE


def sign_policy_document(policy_document):
    """ Sign and return the policy doucument for a simple upload.
    http://aws.amazon.com/articles/1434/#signyours3postform
    """
    policy = base64.b64encode(json.dumps(policy_document).encode())
    signature = base64.b64encode(hmac.new(settings.AWS_CLIENT_SECRET_KEY, policy, hashlib.sha1).digest())
    return {
        'policy': policy.decode(),
        'signature': signature.decode()
    }


def sign_headers(headers):
    """ Sign and return the headers for a chunked upload. """
    return {
        'signature': base64.b64encode(hmac.new(settings.AWS_CLIENT_SECRET_KEY, headers, hashlib.sha1).digest())
    }
